import java.util.Random

fun feedTheFish(){
    val day = randomDay()
    val food = fishFood(day)
    println("today is $day and the fish eat $food")
    println("Change water: ${shouldChangeWater(day)}")
}

fun shouldChangeWater(day: String,temperature: Int=22, dirty: Int=20): Boolean {
    return when{
        isTooHot(temperature)-> true
        isDirty(dirty) -> true
        isSunday(day) -> true
        else -> false
    }
}

fun isSunday(day: String) = day == "Sunday"

fun isDirty(dirty: Int) = dirty > 30

fun isTooHot(temperature: Int) = temperature > 30

fun fishFood(day: String): String {
    return when(day){
        "Monday" -> "flakes"
        "Tuesday" -> "pellets"
        "Wednesday" -> "redworms"
        "Thursday" -> "granules"
        "Friday" -> "mosquitoes"
        else -> "nothing :("
    }
}

fun randomDay(): String {
    val week = arrayOf ("Monday", "Tuesday", "Wednesday", "Thursday",
        "Friday", "Saturday", "Sunday")
    return week[Random().nextInt(week.size)]
}

fun main(args:Array<String>){
    val decorations = listOf ("rock", "pagoda", "plastic plant", "alligator", "flowerpot")
    feedTheFish()
    println(decorations.filter { it[0] == 'f' })
    val eager = decorations.filter { it[0] == 'p' }
    println(eager)
    val filtered = decorations.asSequence().filter { it[0] == 'p' }
    println("Filtered: $filtered")
    val newList = filtered.toList()
    println("newList: $newList")
    val lazyMap = decorations.asSequence().map {
        println("access: $it")
        it
    }

    println("lazy: $lazyMap")
    println("-------")
    println("First: ${lazyMap.first()}")
//    println("-------")
//    println("all: ${lazyMap.toList()}")
//    println("-------")
}


